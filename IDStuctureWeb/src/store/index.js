import axios from 'axios';
import { ElMessage } from 'element-plus'
let url='http://127.0.0.1:8090'


export default {
    data() {
        return {
            //给vue定义变量
            username: '',
            password: '',
            text: '&nbsp;&nbsp;&nbsp;&nbsp;',
            isShow: true,
            bannerList: "",
            loginForm: {
                no: '',
                password: ''
            },
            reUsername:'',
            rePassword:'',
            reOkPassword:''
        };
    },
    methods: {//给vue定义方法
        loginStart(){
            axios.post(url+'/user/login',this.loginForm).then(res=>res.data).then(res=>{
                if (res.code===200){
                    sessionStorage.setItem("CurUser", JSON.stringify(res.data))
                    //跳转到主页
                    this.$router.replace('/Index');
                }else
                    this.$message({
                        type:"error",
                        message: '校验失败，用户名或密码错误！'
                    })
            })
        },
    }
}