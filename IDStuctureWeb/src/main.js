import {createApp} from 'vue'

import App from './App.vue'
import ElementPlus from 'element-plus'

//element-plus
import 'element-plus/theme-chalk/index.css'
import locale from "element-plus/lib/locale/lang/zh-cn";

//全局属性
import './assets/global.css'

//动态路由
import router from './router/login.js'

//图标
import "echarts"
import ECharts from "vue-echarts";



const app = createApp(App)

app.use(router);
app.component('ECharts',ECharts)
app.use(ElementPlus,{locale})


app.mount('#app')
