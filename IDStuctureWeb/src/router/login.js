import {createRouter, createWebHashHistory, createWebHistory} from 'vue-router'
// 1. 定义路由组件.
// 也可以从其他文件导入
import Index from '../components/Index.vue'
import Login from '../components/LoginPage.vue'
import Home from '../components/Home.vue'

// 2. 定义一些路由
// 每个路由都需要映射到一个组件。
// 我们后面再讨论嵌套路由。
const routes = [

    {path: '/', redirect: 'login'},
    {
        //使用正则的方式匹配任意的
        path: '/:path(.*)',
        component: () => import('../components/Error.vue')
    },
    {
        path: '/login',
        component: Login,
        name: 'login'
    },
    {
        path: '/index',
        component: Index,
        name: 'index',
        children: [{
            path: '/home',
            component: Home,
            name: 'Home',
            meta: {
                title: "首页"
            }
        },
            {
                path: '/MainMenu',
                component: () => import('../components/admin/MainMenu.vue'),
                name: 'MainMenu'
            },{
                path: '/identityTable',
                component: () => import('../components/admin/identityTable.vue'),
                name: 'identityTable'
            },
            {
                path: '/randMenu',
                component: () => import('../components/admin/RandMenu.vue'),
                name: 'randMenu'
            },

            {
                path: '/SchoolLocation',
                component: () => import('../components/commonly/SchoolLocation.vue'),
                name: 'SchoolLocation'
            },
            {
                path: '/hfm',
                component: () => import('../components/admin/HFM.vue'),
                name: 'hfm'
            },
            {
                path: '/SmallTr',
                component: () => import('../components/commonly/SmallTr.vue'),
                name: 'SmallTr'
            },
            {
                path: '/WalkAlon',
                component: () => import('../components/commonly/WalkAlon.vue'),
                name: 'WalkAlon'
            },

        ]
    },

]


const router = createRouter({
    // 4. 内部提供了 history 模式的实现。为了简单起见，我们在这里使用 hash 模式。
    history: createWebHistory(),
    routes,
})

//路由守卫
router.beforeEach((to,from,next)=>{
    if (!sessionStorage.getItem("CurUser")){
        if (to.name=="login"){
            next();
        }else {
            router.push("/login")
        }
    }else {
        next();
    }
})

//导出文件
export default router
