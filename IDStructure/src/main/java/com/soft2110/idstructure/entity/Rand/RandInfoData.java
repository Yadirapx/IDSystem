package com.soft2110.idstructure.entity.Rand;

import java.text.SimpleDateFormat;
import java.util.Date;

public class RandInfoData {
    public static int MALE = 0;
    public static int FEMALE = 1;

    public static int getID() {//返回id序号
        return getSectionNumber(1, 5000);
    }

    public static int getSectionNumber(int min, int max) {//生成[min,max]范围的数
        return min + (int) (Math.random() * (max - min));
    }

    public static String identityCard(int sex) {//0代表男 1代表女
        String s="";
        //前6位
        for (int i = 0; i < 6; i++) {
            int number=getSectionNumber(0,9);
            while (number==0&&i==0){
                number=getSectionNumber(0,9);
            }
            s+=number;
        }
        //出生年月日
        s+= getBirth("1974-01-01", "2022-10-30");
        //三位
        for (int i = 0; i < 3; i++) {
            s+=getSectionNumber(0,9);
        }
        //男女
        int number=0;
        do {
            number = getSectionNumber(0,9);
        }while (number%2==sex);
        s+=number;
        //最后一位
        s+=getSectionNumber(0,9);
        return s;
    }

    /**
     * 获取随机日期
     *
     * @param beginDate 起始日期，格式为：yyyy-MM-dd
     * @param endDate   结束日期，格式为：yyyy-MM-dd
     * @return
     */

    private static Date randomDate(String beginDate, String endDate) {
        try {
            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
            Date start = format.parse(beginDate);// 构造开始日期
            Date end = format.parse(endDate);// 构造结束日期
            // getTime()表示返回自 1970 年 1 月 1 日 00:00:00 GMT 以来此 Date 对象表示的毫秒数。
            if (start.getTime() >= end.getTime()) {
                return null;
            }
            long date = random(start.getTime(), end.getTime());

            return new Date(date);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    private static long random(long begin, long end) {
        long rtn = begin + (long) (Math.random() * (end - begin));
        // 如果返回的是开始时间和结束时间，则递归调用本函数查找随机值
        if (rtn == begin || rtn == end) {
            return random(begin, end);
        }
        return rtn;
    }

    //返回生日
    public static String getBirth(String beginDate, String endDate) {
        Date randomDate = randomDate(beginDate, endDate);
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");//要标准话的格式
        String nowStr = sdf.format(randomDate);
        return nowStr;
    }




}
