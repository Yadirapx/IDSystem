package com.soft2110.idstructure.entity.LinkedList;

import lombok.Data;

@Data
public class UserLinkNode<User> {
    private User data;
    UserLinkNode<User> next;
    public UserLinkNode(){
        next=null;
    }
    public UserLinkNode(User d){
        data=d;
        next=null;
    }
}
