package com.soft2110.idstructure.entity.hfm;

import lombok.Data;

@Data
public class HuffmanReceive {
    private String content;
}
