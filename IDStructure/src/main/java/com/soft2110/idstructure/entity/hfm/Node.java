package com.soft2110.idstructure.entity.hfm;

public class Node implements Comparable<Node> {
    public Byte data;//存放数据（字符）本身 ，比如'a'=>97  ''=>32
    public int weight;//权值
    public Node left;
    public Node right;

    public Node(Byte data, int weight) {
        this.data = data;
        this.weight = weight;
    }


    //怎么理解compareTo接口  看:https://blog.csdn.net/Fly_as_tadpole/article/details/87566011
    /*
     * 它返回三种 int 类型的值： 负整数，零 ，正整数。
          返回值    含义
          负整数    当前对象的值 < 比较对象的值 ， 位置排在前
          零       当前对象的值 = 比较对象的值 ， 位置不变
          正整数    当前对象的值 > 比较对象的值 ， 位置排在后
      *
    * */
    @Override
    public int compareTo(Node o) {
        //从小到大进行排序
        return this.weight - o.weight;
    }


}