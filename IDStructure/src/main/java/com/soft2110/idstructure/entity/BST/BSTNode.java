package com.soft2110.idstructure.entity.BST;

public class BSTNode {
    public Identity identity;                      //存放关键字,假设关键字为int类型
    public BSTNode lchild;                        //存放左孩子指针
    public BSTNode rchild;                        //存放右孩子指针

    public BSTNode() {
        lchild = rchild = null;
    }
}
