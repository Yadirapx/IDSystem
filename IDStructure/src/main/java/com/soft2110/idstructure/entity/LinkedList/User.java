package com.soft2110.idstructure.entity.LinkedList;/*
    @Author:***
    @Date:2022/11/18
    @Description：IntelliJ IDEA
*/

import com.baomidou.mybatisplus.annotation.TableField;
import lombok.Data;



@Data
public class User {
    private int id;
    private String no;
    private String name;
    private String password;
    @TableField("role_id")
    private String roleId;

    public User(int id, String no, String name, String password, String roleId) {
        this.id = id;
        this.no = no;
        this.name = name;
        this.password = password;
        this.roleId = roleId;
    }
    public User(){};
}
