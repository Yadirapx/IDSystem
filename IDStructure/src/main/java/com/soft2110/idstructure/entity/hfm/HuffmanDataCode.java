package com.soft2110.idstructure.entity.hfm;


import lombok.Data;

@Data
public class HuffmanDataCode {
    private String content;
    private int zero;
}
