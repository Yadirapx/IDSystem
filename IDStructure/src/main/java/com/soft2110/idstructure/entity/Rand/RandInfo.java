package com.soft2110.idstructure.entity.Rand;

import lombok.Data;

/**
 * 生成随机信息的代码
 *
 * @author 35705
 * @date 2022/12/15
 */
@Data
public class RandInfo {
    private int id;
    private String surname;
    private String name;
    private String address;
}
