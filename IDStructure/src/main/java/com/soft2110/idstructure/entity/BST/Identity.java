package com.soft2110.idstructure.entity.BST;
/*
    @Author:***
    @Date:2022/11/19
    @Description：IntelliJ IDEA
*/

import com.baomidou.mybatisplus.annotation.TableField;
import lombok.Data;

@Data
public class Identity {
    @TableField("id")
    private int id;
    @TableField("name")
    private String name;
    @TableField("gender")
    private int gender;
    @TableField("birthDay")
    private String birthDay;
    @TableField("address")
    private String address;
    @TableField("idNumber")
    private String idNumber;



}
