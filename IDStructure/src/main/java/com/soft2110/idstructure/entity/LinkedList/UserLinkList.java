package com.soft2110.idstructure.entity.LinkedList;
public class UserLinkList                //单链表泛型类
{
	private UserLinkNode<User> head;							//存放头结点

	public UserLinkNode<User> getHead() {
		return head;
	}

	public void setHead(UserLinkNode<User> head) {
		this.head = head;
	}

	public UserLinkList()						//构造方法
	{
		head=new UserLinkNode<User>();					//创建头结点
		head.setNext(null);
	}
	private UserLinkNode<User> geti(int i)				//返回序号为i的结点
	{
		UserLinkNode<User> p=head;
		int j=-1;
		while (j<i)
		{
			j++;
			p=p.getNext();
		}
		return p;
	}
	//线性表的基本运算算法	

	public void CreateList(User[] a)
	{
		UserLinkNode<User> s,t;
		t=head;									//t始终指向尾结点,开始时指向头结点
		for (int i=0;i<a.length;i++) {			//循环建立数据结点s
			s=new UserLinkNode<User>(a[i]);			//新建存放a[i]元素的结点s
			t.setNext(s);							//将s结点插入t结点之后
			t=s;
		}
		t.setNext(null);							//将尾结点的next字段置为null
	}

	public void add(User e)						//在线性表的末尾添加一个元素e
	{
		UserLinkNode<User> s=new UserLinkNode<User>(e);		//新建结点s
		UserLinkNode<User> p=head;
		while (p.getNext()!=null)					//查找尾结点p
			p=p.getNext();
		p.setNext(s);						//在尾结点之后插入结点s
	}

	public int size()							//求线性表长度
	{
		UserLinkNode p=head;
		int cnt=0;
		while (p.getNext()!=null)					//找到尾结点为止
		{
			cnt++;
			p=p.getNext();
		}
		return cnt;
	}

	public User getElem(int i)						//返回线性表中序号为i的元素
	{
		int len=size();
		if (i<0 || i>len-1)
			throw new IllegalArgumentException("查找:位置i不在有效范围内");
		UserLinkNode<User> p=geti(i);					//找到序号为i的结点p
		return p.getData();
	}


	public int findID(User e)						//查找第一个为e的元素的序号
	{
		int j=0;
		UserLinkNode<User> p=head.getNext();
		while (p!=null && p.getData().getId()!=e.getId())
		{
			j++;								//查找元素e
			p=p.getNext();
		}
		if (p==null)							//未找到时返回-1
			return -1;
		else
			return j;							//找到后返回其序号
	}
	public int findID(int e)						//查找第一个为e的元素的序号
	{
		int j=0;
		UserLinkNode<User> p=head.getNext();
		while (p!=null && p.getData().getId()!=e)
		{
			j++;								//查找元素e
			p=p.getNext();
		}
		if (p==null)							//未找到时返回-1
			return -1;
		else
			return j;							//找到后返回其序号
	}

	public void delete(int i) 					//在线性表中删除序号i位置的元素
	{
		if (i<0 || i>size()-1)					//参数错误抛出异常
			throw new IllegalArgumentException("删除:位置i不在有效范围内");
		UserLinkNode<User> p=geti(i-1);				//找到序号为i-1的结点p
		p.setNext(p.getNext().getNext());				//删除p结点的后继结点 p.next=p.next.next
	}

	//更新节点
	public void update(int i,User user){
		UserLinkNode<User> p=geti(i-1);
		p.getNext().setData(user);
	}

	public int checkAccount(User user){
		int j=0;
		UserLinkNode<User> p=head.getNext();
		while (p!=null && !p.getData().getNo().equals(user.getNo()))
		{
			j++;								//查找元素e
			p=p.getNext();
		}
		if (p==null)							//未找到时返回-1
			return -1;
		else
		{
			if (p.getData().getPassword().equals(user.getPassword()))
				return j;
			else return -1;
		}
	}

	//统计相同的元素值
	public int getRoleId(String i){
		int index=0;
		UserLinkNode<User> p=head.getNext();
		while (p!=null)
		{
			if (p.getData().getRoleId().equals(i))
				index++;
			p=p.getNext();
		}
		return index;
	}

	//查找searchId
	public User searchId(String no){
		UserLinkNode<User> p=head.getNext();
		while (p!=null&&!p.getData().getNo().equals(no)){
			p=p.getNext();
		}
		if (p==null){
			User user=new User();
			user.setId(-1);
			return user;
		}
		else
			return p.getData();
	}


}