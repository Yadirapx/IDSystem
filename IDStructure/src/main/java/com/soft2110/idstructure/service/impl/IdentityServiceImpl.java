package com.soft2110.idstructure.service.impl;/*
    @Author:***
    @Date:2022/11/18
    @Description：IntelliJ IDEA
*/

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.soft2110.idstructure.entity.BST.Identity;
import com.soft2110.idstructure.mapper.IdentityMapper;
import com.soft2110.idstructure.service.IdentityService;
import org.springframework.stereotype.Service;

@Service
public class IdentityServiceImpl extends ServiceImpl<IdentityMapper, Identity> implements IdentityService {
   

}
