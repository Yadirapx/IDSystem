package com.soft2110.idstructure.service;/*
    @Author:***
    @Date:2022/11/18
    @Description：IntelliJ IDEA
*/

import com.baomidou.mybatisplus.extension.service.IService;
import com.soft2110.idstructure.entity.Rand.RandInfo;

public interface RandInfoService extends IService<RandInfo> {
    public RandInfo getRand();
}
