package com.soft2110.idstructure.service.impl;

import com.soft2110.idstructure.entity.hfm.HuffmanData;
import com.soft2110.idstructure.entity.hfm.Node;

import java.util.*;


public class HuffmanCodeImpl {
    /**
     * 哈夫曼压缩
     *
     * @param bytes 原始的字符串对应的字节数组
     * @return 是经过哈夫曼编码处理后的字节数组(压缩后的数组)
     */
    public static HuffmanData huffmanZip(byte[] bytes) {
        List<Node> nodes = getNodes(bytes);
        //根据nodes创建哈夫曼树
        Node huffmanTreeRoot = createHuffmanTree(nodes);
        //对应的哈夫曼编码（根据哈夫曼树）
        Map<Byte, String> huffmanCodes = getCodes(huffmanTreeRoot);
        System.out.println(huffmanCodes);
        //根据生成的哈夫曼编码，压缩得到压缩后的哈夫曼字节数组
        HuffmanData huffmanData=zip(bytes, huffmanCodes);
        return huffmanData;

    }

    //接受字节数组 返回List
    private static List<Node> getNodes(byte[] bytes) {

        //1,创建一个ArrayList
        ArrayList<Node> nodes = new ArrayList<Node>();

        //创建bytes，统计每一个byte出现的次数->map[key,value]
        Map<Byte, Integer> counts = new HashMap<>();
        for (byte b : bytes) {//遍历数据
            //Map还没有这个字符数据，第一次
            counts.merge(b, 1, Integer::sum);//它将新的值赋值到 key （如果不存在）或更新给定的key 值对应的 value

            /*
            *  if (count==null){//Map还没有这个字符数据，第一次
                counts.put(b,1);
            }else {
                counts.put(b,count+1);
            }
             */
        }

        //把每一个键值对转换成一个Node对象，并加入到nodes集合
        //遍历map
        //Map.Entry的作用：返回{key,value}的键值对
        for (Map.Entry<Byte, Integer> entry : counts.entrySet()) {
            nodes.add(new Node(entry.getKey(), entry.getValue()));
        }
        return nodes;
    }

    //可以通过list创建相应的赫夫曼树
    private static Node createHuffmanTree(List<Node> nodes) {
        while (nodes.size() > 1) {
            //排序  从小到达
            Collections.sort(nodes);
            //取出第一颗最小的二叉树节点
            Node leftNode = nodes.get(0);
            //去除第二颗最小的二叉树节点
            Node rightNode = nodes.get(1);
            //创建一颗新的二叉树，它的根节点没有data，只有权值
            Node parent = new Node(null, leftNode.weight + rightNode.weight);
            parent.left = leftNode;
            parent.right = rightNode;
            //将已经处理的二叉树从nodes删除
            nodes.remove(leftNode);
            nodes.remove(rightNode);
            //将新的二叉树，加入到nodes
            nodes.add(parent);
        }
        //nodes最后的节点，就是哈夫曼的根节点
        return nodes.get(0);
    }


//生成哈夫曼对应的哈夫曼编码
    //思路：
    //1，将哈夫曼编码存放在Map<Bytes,String> 形式
    //  {32=001, 97=0100, 100=011000, 117=011001, 101=01110, 118=011011, 105=0101, 121=011010, 106=00010, 107=01111, 108=0000, 111=00011}
    private static Map<Byte, String> huffmanCodes;
//2，在生成和哈夫曼编码表示，需要去拼接路径，定义一个StringBuilder 存储某个叶子节点的路径
    private static StringBuilder stringBuilder;


    //为了调用方便，我们重载getCodes
    private static Map<Byte, String> getCodes(Node root) {
        huffmanCodes=new HashMap<>();
        stringBuilder=new StringBuilder();
        if (root == null)
            return null;
        //处理root的左子树
        getCodes(root.left, "0", stringBuilder);
        //处理root的右子树
        getCodes(root.right, "1", stringBuilder);
        return huffmanCodes;
    }


    /**
     * 功能：将传入的node节点的所有叶子节点的哈夫曼编码得到，并传入huffmancodes集合
     *
     * @param node          传入节点
     * @param code          路径：左子树节点是0,右子树节点是1
     * @param stringBuilder 用于拼接路径
     */
    private static void getCodes(Node node, String code, StringBuilder stringBuilder) {
        StringBuilder stringBuilder1 = new StringBuilder(stringBuilder);
        //将code加入到stringbulider1
        stringBuilder1.append(code);
        if (node != null) {//如果node==null就不处理
            //判断当前node是叶子节点还是非叶子节点
            if (node.data == null) {//非叶子节点
                //递归处理
                //向左递归
                getCodes(node.left, "0", stringBuilder1);
                //向右递归
                getCodes(node.right, "1", stringBuilder1);
            } else {//说明它是叶子节点
                huffmanCodes.put(node.data, stringBuilder1.toString());
            }

        }
    }


    /**
     * 编写一个方法，将字符串对应的bytes[]数组，通过生成的哈夫曼编码，返回一个哈夫曼编码 压缩后的byte[]
     *
     * @param bytes        这是原始的字符串对应的bytes[]
     * @param huffmanCodes 生成的哈夫曼树编码map
     * @return 返回处理后的byte[]
     * 举例: String content = "i like like like java do you like a java"; =>byte[] contentByte = content.getBytes();
     * 返回字符串""
     * =>对应的byte[] hufumanCodeBytes,即8位对应一个byte，放入到huffmanCodeBytes
     * huffmanCodeBytes[0]
     */
    private static HuffmanData zip(byte[] bytes, Map<Byte, String> huffmanCodes) {
        //1,先利用huffmanCodes将bytes转成哈夫曼编码对应的字符串
        //StringBuffer 和 StringBuilder类的对象能够被多次的修改，并且不产生新的未使用对象。
        //StringBuffer 线程不安全 常用方法 append 和 insert
        StringBuilder stringBuilder = new StringBuilder();
        //遍历bytes数组
        for (byte b : bytes) {
            stringBuilder.append(huffmanCodes.get(b));
        }
        System.out.println("编码数据"+stringBuilder);

        //统计返回byte[] huffmanCodeBytes长度
        //一句话int len=(stringBuilder.length()+7)/8;
        int len;
        //判断len是否是8的倍数
        if (stringBuilder.length() % 8 == 0) {
            len = stringBuilder.length() / 8;
        } else {
            len = stringBuilder.length() / 8 + 1;
        }
        //创建 存储压缩后的byte数组
        byte[] HuffmanCodesBy = new byte[len];
        HuffmanData huffmanData=new HuffmanData(len);
        for (int i = 0; i < stringBuilder.length(); i += 8) {//因为是8位对应的一个byte，所以步长+8
            String strByte;
            if (i + 8 > stringBuilder.length())//不够8位
            {
                strByte = stringBuilder.substring(i);
            }
            else
                strByte = stringBuilder.substring(i, i + 8);
            //将strByte转换成string【】数组中，放入huffmanCodeBytes
            huffmanData.add(String.valueOf(Integer.parseInt(strByte, 2)));
            //最后一位字符串如果是正数且长度小于8
            if (strByte.length()<8&&strByte.charAt(0)=='0') {
                System.out.println(strByte.length());
                int j=0;
                while (strByte.charAt(j)!='1'&&j<strByte.length()-1)
                    j++;
                huffmanData.zero=j;
            }

        }
        return huffmanData;
    }



    //完成数据的解压
    //思路
    //1，将huffmanCodesBy:[-88, -65, -56, -65, -56, -65, -55, 77, -57, 6, -24, -14, -117, -4, -60, -90, 28]
    //重新先转换成哈夫曼对应的二进制字符串“1010100010111111...”
    //2，哈夫曼对应的二进制的字符串"1010100010111111..."=>对照哈夫曼编码=>"i like like like java do you like a java"

    /**
     * 将一个byte转化成一个二进制字符串
     *
     * @param b    传入的byte
     * @param flag 标志是否需要补高位，如果是true则需要，如果是最后一位无需补高位
     * @return 是该b 对应的二进制字符串，（注意按补码返回）
     */
    private static String byteToBitString(boolean flag,int b){
        //使用变量保存b
        //如果是正数我们还存在补高位
        if (flag){
            b |=256;//按位与  256  1 0000  0000 |  0000 0001 =>1 0000 0001
        }
        String str=Integer.toBinaryString(b);//返回是temp对应的二进制的补码
        if (flag){
            return str.substring(str.length()-8);
        }else {
            return str;
        }
    }

    /**
     * 解码
     *
     * @param huffmanData 霍夫曼编码得到的字节组
     * @return 就是原来字符串对应的数组
     *///编写一个方法，完成对压缩数据的解码
    public static byte[] decode(HuffmanData huffmanData){
        String[] huffmanBytes=huffmanData.HuffmanCodesBy;
        //1,先得到huffmanBytes对应的二进制字符串，形式"101010001011"
        StringBuilder stringBuilder=new StringBuilder();
        //将byte数组转换成二进制字符串
        for (int i = 0; i < huffmanBytes.length; i++) {
            int huffmanBytesInt = Integer.parseInt(huffmanBytes[i]);
            //判断是不是最后一个字节
            boolean flag=(i==huffmanBytes.length-1);
            String  cost=byteToBitString(!flag, huffmanBytesInt);

            //前缀为0的值
            String  zeroCost="";
            if (cost.length()!=8) {
                for (int j = 0; j < huffmanData.zero; j++) {
                    zeroCost +='0';
                }
            }


            stringBuilder.append(zeroCost+cost);
        }
        System.out.println("解码数据"+stringBuilder.toString());
        System.out.println(huffmanCodes);
        //把字符串按照指定的哈夫曼编码进行解码
        //把哈夫曼编码进行调换，因为反向查询a->100 100->a
        Map<String,Byte> map=new HashMap<String,Byte>();
        for (Map.Entry<Byte,String> entry:huffmanCodes.entrySet()) {
            map.put(entry.getValue(),entry.getKey());
        }
        System.out.println(map);
        //创建要给的集合，存放byte
        List<Byte> list=new ArrayList<>();
        //i可以理解就是索引，扫描stringBuilder
        for (int i=0;i<stringBuilder.length();){
            int count=1;//小的计数器
            boolean flag=true;
            Byte b=null;
            while (flag){
                //101010001
                //递增的取出 key 1
                String key=stringBuilder.substring(i,i+count);//i不动，让count移动，指定匹配到一个字符
                b=map.get(key);
                if (b==null){//说明没有匹配到
                    count++;
                }else {
                    //匹配到
                    flag=false;
                }
            }
            list.add(b);
            i+=count;//i一直移动到count
        }
        //for循环结束，我们list中存放了所有的数据""
        //把list中的数据放入byte[]并返回

        byte[] b =new byte[list.size()];
        for (int i=0;i< b.length;i++){
            b[i]=list.get(i);
        }
        return b;
    }
}

