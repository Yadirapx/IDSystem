package com.soft2110.idstructure.service.impl;/*
    @Author:***
    @Date:2022/11/18
    @Description：IntelliJ IDEA
*/

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.soft2110.idstructure.entity.LinkedList.User;
import com.soft2110.idstructure.mapper.UserMapper;
import com.soft2110.idstructure.service.UserService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service
public class UserServiceImpl extends ServiceImpl<UserMapper, User> implements UserService {
    @Resource
    private UserMapper userMapper;
    @Override
    public List<User> selectAll() {
        return userMapper.selectAll();
    }

    @Override
    public IPage pageC(IPage<User> page) {
        return userMapper.pageC(page);
    }

    @Override
    public IPage pageCC(IPage<User> page, Wrapper Wrapper) {
        return userMapper.pageCC(page,Wrapper);
    }


}
