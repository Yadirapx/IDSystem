package com.soft2110.idstructure.service.impl;

import com.soft2110.idstructure.entity.LinkedList.User;
import com.soft2110.idstructure.entity.LinkedList.UserLinkList;
import com.soft2110.idstructure.service.UserLinkListService;
import com.soft2110.idstructure.entity.Rand.RandInfoData;

import java.util.ArrayList;
import java.util.List;

public class UserLinkNodeImpl implements UserLinkListService {
    private boolean first=true;
    UserLinkList usersLink;

    public UserLinkNodeImpl(){
        usersLink=new UserLinkList();
        User[] users=new User[]{};
        usersLink.CreateList(users);
    }

    public int size(){
        return usersLink.size();
    }

    @Override
    public void add(User user) {
        user.setId(RandInfoData.getID());
        usersLink.add(user);
    }

    @Override
    public void delete(int id) {
        int i = usersLink.findID(id);
        usersLink.delete(i);
    }

    @Override
    public void update(User user) {
        int i=usersLink.findID(user);
        usersLink.update(i,user);
    }

    public List<User> list(List<User> listUser){
       if (first){//第一次创建树
           User[] users=new User[listUser.size()];//设置属性
           for (int i = 0; i < listUser.size(); i++) {
               users[i]=listUser.get(i);
           }
           usersLink.CreateList(users);  //创建userLink表
       }
       first=false;
       return userListToList(usersLink);
    }

    //创建单链表
    public int login(User user){
        return usersLink.checkAccount(user);
    }


    //将usersLink对象转换成List<User>
    private List<User> userListToList(UserLinkList users){
        List<User> list=new ArrayList<>();
        for (int i = 0; i < users.size(); i++) {
            list.add(users.getElem(i));
        }
        return list;
    }

    public User getElement(int i){
        return usersLink.getElem(i);
    }


    public int getRoleId(){
        return usersLink.getRoleId("0");
    }

    public User searchId(String no){
        return usersLink.searchId(no);
    }



}
