package com.soft2110.idstructure.service.impl;/*
    @Author:***
    @Date:2022/11/18
    @Description：IntelliJ IDEA
*/

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.soft2110.idstructure.entity.Rand.RandInfo;
import com.soft2110.idstructure.mapper.RandInfoMapper;
import com.soft2110.idstructure.service.RandInfoService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

@Service
public class RandInfoServiceImpl extends ServiceImpl<RandInfoMapper, RandInfo> implements RandInfoService {

    @Resource
    private RandInfoMapper randInfoMapper;

    @Override
    public RandInfo getRand() {
        return randInfoMapper.getRand();
    }
}
