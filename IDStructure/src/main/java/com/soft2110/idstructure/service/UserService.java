package com.soft2110.idstructure.service;/*
    @Author:***
    @Date:2022/11/18
    @Description：IntelliJ IDEA
*/

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.soft2110.idstructure.entity.LinkedList.User;

import java.util.List;

public interface UserService extends IService<User> {
    public List<User>  selectAll();

    IPage pageC(IPage<User> page);

    IPage pageCC(IPage<User> page, Wrapper Wrapper);
}
