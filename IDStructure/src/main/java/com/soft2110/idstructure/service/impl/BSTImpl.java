package com.soft2110.idstructure.service.impl;


import com.soft2110.idstructure.entity.BST.BSTNode;
import com.soft2110.idstructure.entity.BST.Identity;

import java.util.ArrayList;

public class BSTImpl {
    public BSTNode r;                        //二叉排序树根结点


    public BSTImpl() {
        r = null;//默认为空树
    }

    /**
     * 插入二叉树
     *
     * @param k 一个变量
     */
    public void add(Identity k)                //插入一个关键字为k的结点
    {
        if (r==null){//判断根节点是否位空
            BSTNode bstNode=new BSTNode();
            bstNode.identity=k;
            r=bstNode;
        }else {
            InsertBST(r, k);
        }
    }

    private BSTNode InsertBST(BSTNode p, Identity k) //在以p为根的BST中插入关键字为k的结点
    {
        if (p == null)                           //循环结束条件
        {
            p = new BSTNode();
            p.identity = k;
        } else if (k.getId() < p.identity.getId())
            p.lchild = InsertBST(p.lchild, k);    //插入到p的左子树中
        else if (k.getId() > p.identity.getId())
            p.rchild = InsertBST(p.rchild, k);    //插入到p的右子树中
        return p;
    }

     //查找id是否存在
    public boolean SearchById(int k){
        return SearchById1(r,k);
    }

    private boolean SearchById1(BSTNode p,int k){
        if (p==null) return false;
        if (p.identity.getId()==k) return true;
        if(k<p.identity.getId())
            return SearchById1(p.lchild,k);
        else return SearchById1(p.rchild,k);
    }

    /**
     * 更新值
     *
     * @param k            原来值
     * @param replaceValue 替换值
     * @return boolean  替换成功返回true 否则返回false
     */
    public boolean updateBST(int k, Identity replaceValue) {
        return updateBST1(r, k, replaceValue);
    }

    private boolean updateBST1(BSTNode p, int k, Identity replaceValue) {
        if (p == null) return false;
        if (p.identity.getId() == k) {
            p.identity.setId(replaceValue.getId());
            p.identity.setGender(replaceValue.getGender());
            p.identity.setName(replaceValue.getName());
            p.identity.setBirthDay(replaceValue.getBirthDay());
            p.identity.setAddress(replaceValue.getAddress());
            p.identity.setIdNumber(replaceValue.getIdNumber());
            return true;
        }
        if (k < p.identity.getId())
            return updateBST1(p.lchild, k, replaceValue);        //在左子树中递归查找
        else
            return updateBST1(p.rchild, k, replaceValue);        //在右子树中递归查找
    }

    /**
     * 删除元素
     *
     * @param k 一个整形
     * @return boolean
     */

    private BSTNode f=null;                              //用于存放待删除结点的双亲结点
    public boolean DeleteBST(int k)                //删除关键字为k的结点
    {
        if (r == null)                             //如果r为null代表为空树
            return false;
        return DeleteBST1(r, k, -1);                        //r为二叉排序树的根结点
    }


    //匹配节点所在地
    private boolean DeleteBST1(BSTNode p, int k, int flag)    //被DeleteBST方法调用，flag=0，p为双亲f的左孩子，flag=1，p为双亲f的右孩子
    {
        if (p.identity.getId() == k)
            return DeleteNode(p, f, flag);        //找到后删除p结点
        if (k < p.identity.getId()) {
            f = p;
            return DeleteBST1(p.lchild, k, 0);
        } else {
            f = p;
            return DeleteBST1(p.rchild, k, 1);
        }
    }

    private boolean DeleteNode(BSTNode p, BSTNode f, int flag)    //删除结点p（其双亲为f）
    {
        if (p.rchild == null)                        //结点p只有左孩子(含p为叶子的情况)：将p的左孩子替代p
        {
            if (flag == -1)                        //结点p的双亲为空(p为根结点)
                r = p.lchild;                        //修改根结点r为p的左孩子
            else if (flag == 0)                    //p为双亲f的左孩子
                f.lchild = p.lchild;                //将f的左孩子置为p的左孩子
            else                                //p为双亲f的右孩子
                f.rchild = p.lchild;                //将f的右孩子置为p的左孩子
        } else if (p.lchild == null)                //结点p只有右孩子：将p的右孩子替代p
        {
            if (flag == -1)                        //结点p的双亲为空(p为根结点)
                r = p.rchild;                        //修改根结点r为p的右孩子
            else if (flag == 0)                    //p为双亲f的左孩子
                f.lchild = p.rchild;                //将f的左孩子置为p的左孩子
            else                                //p为双亲f的右孩子
                f.rchild = p.rchild;                //将f的右孩子置为p的左孩子
        } else                                    //结点p有左右孩子
        {
            BSTNode f1 = p;
            BSTNode q = p.lchild;                //q转向结点p的左孩子
            if (q.rchild == null)                    //若结点q没有右孩子
            {
                p.identity.setId(q.identity.getId());                  //将被删结点p的值用q的值替代
                p.lchild = q.lchild;                //删除结点q
            } else                                //若结点q有右孩子
            {
                while (q.rchild != null)        //找到最右下结点q,其双亲结点为f1
                {
                    f1 = q;
                    q = q.rchild;
                }
                p.identity.setId(q.identity.getId());                   //将被删结点p的值用q的值替代
                f1.rchild = q.lchild;                //删除结点q
            }
        }
        return true;
    }

    ArrayList<Identity> list;
    //中序遍历
    public ArrayList<Identity> InOrder() {
        list=new ArrayList<>();
        InOrder1(r);
        return list;
    }

    private void InOrder1(BSTNode t) {
        if (t != null) {
            InOrder1(t.lchild);
            list.add(t.identity);
            InOrder1(t.rchild);
        }
    }
}