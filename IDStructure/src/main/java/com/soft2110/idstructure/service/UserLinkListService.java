package com.soft2110.idstructure.service;

import com.soft2110.idstructure.entity.LinkedList.User;

public interface UserLinkListService {
    public void add(User user);
    public void delete(int id);
    public void update(User user);

}
