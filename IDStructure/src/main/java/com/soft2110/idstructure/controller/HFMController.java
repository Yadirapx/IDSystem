package com.soft2110.idstructure.controller;


import com.soft2110.idstructure.common.Result;
import com.soft2110.idstructure.entity.hfm.HuffmanReceive;
import com.soft2110.idstructure.entity.hfm.HuffmanDataCode;
import com.soft2110.idstructure.service.impl.HuffmanCodeImpl;
import com.soft2110.idstructure.entity.hfm.HuffmanData;
import org.springframework.web.bind.annotation.*;


import java.util.Arrays;

@RestController
@RequestMapping("/hfm")
public class HFMController {

    private HuffmanCodeImpl huffmanServer = new HuffmanCodeImpl();//挂载他
    private HuffmanData huffmanCodesBy;

    @PostMapping("/zip")
    public Result code(@RequestBody HuffmanReceive content) {
        try {
            String contentString = content.getContent();
            byte[] contentByte = contentString.getBytes();
            huffmanCodesBy = huffmanServer.huffmanZip(contentByte);
            HuffmanDataCode huffmanDataDecode = new HuffmanDataCode();
            huffmanDataDecode.setContent(Arrays.toString(huffmanCodesBy.HuffmanCodesBy));
            return Result.suc(huffmanDataDecode);
        } catch (NullPointerException e) {
            return Result.fail();
        }
    }

    @PostMapping("/decode")
    public Result decode() {
        try {
            byte[] bytes = huffmanServer.decode(huffmanCodesBy);
            return Result.suc(new String(bytes));
        }catch (NullPointerException e){
            return Result.fail();
        }
    }

}
