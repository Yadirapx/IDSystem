package com.soft2110.idstructure.controller;/*
    @Author:***
    @Date:2022/11/18
    @Description：控制类
*/

import com.soft2110.idstructure.service.impl.BSTImpl;
import com.soft2110.idstructure.common.Result;
import com.soft2110.idstructure.entity.BST.Identity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;


@RestController
@RequestMapping("/identity")
public class IdentityController {

    private boolean first=true;
    private final BSTImpl bt=new BSTImpl();

    //开始获取数据
    @GetMapping("/list")
    public List<Identity> list(){
        ArrayList<Identity> list = bt.InOrder();
        return list;
    }

    //新增
    @PostMapping("/save")
    public Result save(@RequestBody Identity identity) {
        bt.add(identity);
        return  Result.suc();
    }


    @GetMapping("/del")
    public Result del(@RequestParam int id){
        return bt.DeleteBST(id)?Result.suc():Result.fail();
    }

    //更新
    @PostMapping("/update")
    public Result update(@RequestBody Identity identity){
        int id=identity.getId();
        return bt.updateBST(id,identity)?Result.suc():Result.fail();
    }

    @GetMapping("/findById")
    public Result findById(int id){
        return  bt.SearchById(id)?Result.suc():Result.fail();
    }

}
