package com.soft2110.idstructure.controller;

import com.soft2110.idstructure.common.Result;
import com.soft2110.idstructure.entity.LinkedList.User;
import com.soft2110.idstructure.service.UserService;
import com.soft2110.idstructure.service.impl.UserLinkNodeImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;


import java.util.ArrayList;
import java.util.List;


@RestController
@RequestMapping("/user")
public class UserController {
    @Autowired
    private UserService userService;

    private UserLinkNodeImpl userLinkNode=new UserLinkNodeImpl();


    //开始获取数据
    @GetMapping("/list")//这边修改线性结构的存储
    public List<User> list(){
       List<User> list=userLinkNode.list(userService.list());
       return list;
    }

    //新增元素
    @PostMapping("/save")
    public Result save(@RequestBody User user) {
        userLinkNode.add(user);//添加元素
        return Result.suc();
    }

    //更新
    @PostMapping("/update")
    public Result update(@RequestBody User user){
        userLinkNode.update(user);
        return Result.suc();
    }

    //根据id删除
    @GetMapping("/del")
    public Result del(@RequestParam String id){
        userLinkNode.delete(Integer.parseInt(id));
        return Result.suc();
    }


    //登录功能
    @PostMapping("/login")
    public Result login(@RequestBody User user){
        list();//初始化单链表
        System.out.println(userLinkNode.size());
        int i=userLinkNode.login(user);
        if (i==-1) return Result.fail();
        return Result.suc(userLinkNode.getElement(i));
    }

    @GetMapping("/getRold")
    public int getRoleId(){
        return userLinkNode.getRoleId();
    }

    @GetMapping("/searchId")
    public Result searchId(String id){
        User user=userLinkNode.searchId(id);
        List<User> returnList=new ArrayList<>();
        returnList.add(user);
        if (user.getId()==-1)
            return Result.fail();
        else
            return Result.suc(returnList,1L);
    }

}
