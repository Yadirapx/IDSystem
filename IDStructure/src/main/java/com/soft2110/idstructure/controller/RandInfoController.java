package com.soft2110.idstructure.controller;


import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.soft2110.idstructure.common.QueryPageParam;
import com.soft2110.idstructure.common.Result;
import com.soft2110.idstructure.entity.Rand.RandInfoData;
import com.soft2110.idstructure.entity.BST.Identity;
import com.soft2110.idstructure.entity.Rand.RandInfo;
import com.soft2110.idstructure.service.RandInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;


import java.util.List;

@RestController
@RequestMapping("/rand")
public class RandInfoController {

    private String IDNumber;
    @Autowired
    private RandInfoService randInfoService;

    //开始获取数据
    @GetMapping("/list")
    public List<RandInfo> list() {
        return randInfoService.list();
    }

    @GetMapping("/cc")
    public RandInfo getRand() {
        return randInfoService.getRand();
    }

    @GetMapping("/getName")//获取随机用户名哦啦啦啦啦啦啦啦，
    public String getName() {
        String firstName = randInfoService.getRand().getSurname();
        String lastName = randInfoService.getRand().getName();
        return firstName + lastName;
    }

    @GetMapping("/getIDCard")
    public String getIDCard() {
        int gender = RandInfoData.getSectionNumber(0, 1);
        IDNumber = RandInfoData.identityCard(gender);
        return IDNumber;
    }

    @GetMapping("/getAddress")
    public String getAddress() {
        return randInfoService.getRand().getAddress();
    }


    @GetMapping("/getIDData")
    public Identity getIDData() {
        Identity identity = new Identity();
        //设置id
        identity.setId(RandInfoData.getID());
        //设置名字
        identity.setName(getName());
        //设置性别
        int gender = RandInfoData.getSectionNumber(0, 2);
        identity.setGender(gender);
        //设置身份证号
        String idCode=RandInfoData.identityCard(gender);
        identity.setIdNumber(idCode);
        //设置出生年月日
        identity.setBirthDay(idCode.substring(6,14));
        //设置地址
        identity.setAddress(randInfoService.getRand().getAddress());
        return identity;
    }

   //新增
    @PostMapping("/save")
    public Result save(@RequestBody RandInfo randInfo) {

        return randInfoService.save(randInfo)?Result.suc():Result.fail();
    }

    //更新
    @PostMapping("/update")
    public Result update(@RequestBody RandInfo randInfo){
        return randInfoService.updateById(randInfo)?Result.suc():Result.fail();
    }

    //根据id删除
    @GetMapping("/del")
    public Result del(@RequestParam int id){
        return randInfoService.removeById(id)?Result.suc():Result.fail();
    }


     @PostMapping("listPage")
    public Result listPage(@RequestBody QueryPageParam query) {



        //分页代码
        Page<RandInfo> page=new Page<>();
        page.setCurrent(query.getPageNum());//设置第一页
        page.setSize(query.getPageSize());//设置尺寸

        //模糊查询
        LambdaQueryWrapper<RandInfo> lambdaQueryWrapper=new LambdaQueryWrapper<>();

        randInfoService.page(page);

        IPage result = randInfoService.page(page,lambdaQueryWrapper);
//        System.out.println("total=="+result.getTotal());
        return Result.suc( result.getRecords(),result.getTotal());
    }




}
