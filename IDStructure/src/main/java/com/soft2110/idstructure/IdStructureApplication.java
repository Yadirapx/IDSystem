package com.soft2110.idstructure;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class IdStructureApplication {

    public static void main(String[] args) {
        SpringApplication.run(IdStructureApplication.class, args);
    }

}
