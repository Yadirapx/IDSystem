package com.soft2110.idstructure.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.soft2110.idstructure.entity.Rand.RandInfo;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface RandInfoMapper extends BaseMapper<RandInfo> {
    public RandInfo getRand();
}
