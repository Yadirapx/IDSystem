package com.soft2110.idstructure.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import com.soft2110.idstructure.entity.BST.Identity;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface IdentityMapper extends BaseMapper<Identity> {

}
