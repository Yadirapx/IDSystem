package com.soft2110.idstructure.common;/*
    @Author:***
    @Date:2022/11/18
    @Description：IntelliJ IDEA
*/

import lombok.Data;

import java.util.HashMap;


//分页
@Data
public class QueryPageParam {
    private static int PAGE_SIZE = 20;
    private static int PAGE_NUM = 5;

    private int pageSize = PAGE_SIZE;
    private int pageNum = PAGE_NUM;

    private HashMap param=new HashMap<>();
}
