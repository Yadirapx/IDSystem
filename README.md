# IDSystem

#### 介绍
这个项目是数据结构的相关实训代码，我写了一个线性结构(单链表)和两个非线性结构(二叉排序树和哈夫曼树)。

#### 软件架构
IDStructure是后端项目，采用了springboot+mybatis-plus

IDStructureWeb是前端项目，采用了vue3+elementPlus+vite+echarts


#### 安装教程

后端项目IDStructure，用maven添加依赖就能运行

前端项目IDStuctureWeb
1.  cd IDStuctureWeb
2.  npm install
3.  run dev


